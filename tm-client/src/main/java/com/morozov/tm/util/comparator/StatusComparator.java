package com.morozov.tm.util.comparator;

import com.morozov.tm.endpoint.AbstractWorkEntity;

import com.morozov.tm.endpoint.AbstractWorkEntityDto;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class StatusComparator implements Comparator<AbstractWorkEntityDto> {
    @Override
    public int compare(@NotNull final AbstractWorkEntityDto o1, @NotNull final AbstractWorkEntityDto o2) {
        return o1.getStatus().compareTo(o2.getStatus());
    }
}
