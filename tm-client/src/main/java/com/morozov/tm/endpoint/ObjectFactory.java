
package com.morozov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.morozov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccessFirbidenException_QNAME = new QName("http://endpoint.tm.morozov.com/", "AccessFirbidenException");
    private final static QName _CloneNotSupportedException_QNAME = new QName("http://endpoint.tm.morozov.com/", "CloneNotSupportedException");
    private final static QName _UserNotFoundException_QNAME = new QName("http://endpoint.tm.morozov.com/", "UserNotFoundException");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.tm.morozov.com/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.tm.morozov.com/", "closeSessionResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.tm.morozov.com/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.tm.morozov.com/", "openSessionResponse");
    private final static QName _ServerHostInfo_QNAME = new QName("http://endpoint.tm.morozov.com/", "serverHostInfo");
    private final static QName _ServerHostInfoResponse_QNAME = new QName("http://endpoint.tm.morozov.com/", "serverHostInfoResponse");
    private final static QName _ServerPortInfo_QNAME = new QName("http://endpoint.tm.morozov.com/", "serverPortInfo");
    private final static QName _ServerPortInfoResponse_QNAME = new QName("http://endpoint.tm.morozov.com/", "serverPortInfoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.morozov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccessFirbidenException }
     * 
     */
    public AccessFirbidenException createAccessFirbidenException() {
        return new AccessFirbidenException();
    }

    /**
     * Create an instance of {@link CloneNotSupportedException }
     * 
     */
    public CloneNotSupportedException createCloneNotSupportedException() {
        return new CloneNotSupportedException();
    }

    /**
     * Create an instance of {@link UserNotFoundException }
     * 
     */
    public UserNotFoundException createUserNotFoundException() {
        return new UserNotFoundException();
    }

    /**
     * Create an instance of {@link CloseSession }
     * 
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     * 
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     * 
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     * 
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link ServerHostInfo }
     * 
     */
    public ServerHostInfo createServerHostInfo() {
        return new ServerHostInfo();
    }

    /**
     * Create an instance of {@link ServerHostInfoResponse }
     * 
     */
    public ServerHostInfoResponse createServerHostInfoResponse() {
        return new ServerHostInfoResponse();
    }

    /**
     * Create an instance of {@link ServerPortInfo }
     * 
     */
    public ServerPortInfo createServerPortInfo() {
        return new ServerPortInfo();
    }

    /**
     * Create an instance of {@link ServerPortInfoResponse }
     * 
     */
    public ServerPortInfoResponse createServerPortInfoResponse() {
        return new ServerPortInfoResponse();
    }

    /**
     * Create an instance of {@link SessionDto }
     * 
     */
    public SessionDto createSessionDto() {
        return new SessionDto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessFirbidenException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "AccessFirbidenException")
    public JAXBElement<AccessFirbidenException> createAccessFirbidenException(AccessFirbidenException value) {
        return new JAXBElement<AccessFirbidenException>(_AccessFirbidenException_QNAME, AccessFirbidenException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloneNotSupportedException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "CloneNotSupportedException")
    public JAXBElement<CloneNotSupportedException> createCloneNotSupportedException(CloneNotSupportedException value) {
        return new JAXBElement<CloneNotSupportedException>(_CloneNotSupportedException_QNAME, CloneNotSupportedException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "UserNotFoundException")
    public JAXBElement<UserNotFoundException> createUserNotFoundException(UserNotFoundException value) {
        return new JAXBElement<UserNotFoundException>(_UserNotFoundException_QNAME, UserNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServerHostInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "serverHostInfo")
    public JAXBElement<ServerHostInfo> createServerHostInfo(ServerHostInfo value) {
        return new JAXBElement<ServerHostInfo>(_ServerHostInfo_QNAME, ServerHostInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServerHostInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "serverHostInfoResponse")
    public JAXBElement<ServerHostInfoResponse> createServerHostInfoResponse(ServerHostInfoResponse value) {
        return new JAXBElement<ServerHostInfoResponse>(_ServerHostInfoResponse_QNAME, ServerHostInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServerPortInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "serverPortInfo")
    public JAXBElement<ServerPortInfo> createServerPortInfo(ServerPortInfo value) {
        return new JAXBElement<ServerPortInfo>(_ServerPortInfo_QNAME, ServerPortInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServerPortInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "serverPortInfoResponse")
    public JAXBElement<ServerPortInfoResponse> createServerPortInfoResponse(ServerPortInfoResponse value) {
        return new JAXBElement<ServerPortInfoResponse>(_ServerPortInfoResponse_QNAME, ServerPortInfoResponse.class, null, value);
    }

}
