
package com.morozov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for taskDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="taskDto"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.tm.morozov.com/}abstractWorkEntityDto"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idProject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskDto", propOrder = {
    "idProject"
})
public class TaskDto
    extends AbstractWorkEntityDto
{

    protected String idProject;

    /**
     * Gets the value of the idProject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProject() {
        return idProject;
    }

    /**
     * Sets the value of the idProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProject(String value) {
        this.idProject = value;
    }

}
