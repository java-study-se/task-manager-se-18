package com.morozov.tm.configuration;

import com.morozov.tm.endpoint.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.morozov.tm")
public class EndpointConfigurationBean {
    @Bean
    public DomainEndpoint domainEndpoint() {
        return new DomainEndpointService().getDomainEndpointPort();
    }

    @Bean
    public UserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    public ProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public TaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }
}
