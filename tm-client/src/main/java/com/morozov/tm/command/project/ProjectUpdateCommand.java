package com.morozov.tm.command.project;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectUpdateCommand extends AbstractCommand {
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private IServiceLocator serviceLocator;

    public ProjectUpdateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update selected project";
    }

    @Override
    final public void execute() throws ProjectNotFoundException_Exception, CloneNotSupportedException_Exception,
            ParseException_Exception, AccessFirbidenException_Exception, UserNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите ID проекта для изменения");
        @NotNull final String updateProjectId = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое имя проекта");
        @NotNull final String updateProjectName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое описание проекта");
        @NotNull final String updateProjectDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
        @NotNull final String startUpdateDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
        @NotNull final String endUpdateDate = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        projectEndpoint.updateProject(session, updateProjectId, updateProjectName, updateProjectDescription,
                startUpdateDate, endUpdateDate);
        ConsoleHelperUtil.writeString("Проект изменен");
    }
}
