package com.morozov.tm.command.system;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;
@Component
public class ServerInfoCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    SessionEndpoint sessionEndpoint;

    public ServerInfoCommand() {
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ADMIN);

    }

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getDescription() {
        return "Server information";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException, JAXBException, UserNotFoundException_Exception,
            StringEmptyException_Exception, UserExistException_Exception, AccessFirbidenException_Exception,
            CloneNotSupportedException_Exception, RepositoryEmptyException_Exception, ParseException_Exception,
            TaskNotFoundException_Exception, ProjectNotFoundException_Exception, ConnectionLostException_Exception,
            SqlCustomException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
        final String port = sessionEndpoint.serverPortInfo(session);
        final String host = sessionEndpoint.serverHostInfo(session);
        ConsoleHelperUtil.writeString("Server Information:");
        ConsoleHelperUtil.writeString("HOST: " + host);
        ConsoleHelperUtil.writeString("PORT: " + port);
    }
}
