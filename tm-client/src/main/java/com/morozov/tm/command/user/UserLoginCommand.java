package com.morozov.tm.command.user;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserLoginCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    final public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "User authorisation";
    }

    @Override
    final public void execute() throws UserNotFoundException_Exception, StringEmptyException_Exception {
        ConsoleHelperUtil.writeString("Введите логин");
        @NotNull final String login = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите пароль");
        @NotNull final String password = ConsoleHelperUtil.readString();
        if(login.isEmpty() || password.isEmpty()) throw  new StringEmptyException_Exception();
        final SessionDto session = sessionEndpoint.openSession(login, password);
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        serviceLocator.setSession(session);
        ConsoleHelperUtil.writeString("Вы вошли под учетной записью " + login);
    }
}
