package com.morozov.tm.command.task;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskRemoveCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private TaskEndpoint taskEndpoint;

    public TaskRemoveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove selected task";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception, StringEmptyException_Exception,
            CloneNotSupportedException_Exception, UserNotFoundException_Exception, TaskNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите ID задачи для удаления");
        @NotNull final String idDeletedTask = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        if (taskEndpoint.removeTaskById(session, idDeletedTask)) {
            ConsoleHelperUtil.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
        } else {
            ConsoleHelperUtil.writeString("Задачи с данным ID не существует");
        }
    }
}
