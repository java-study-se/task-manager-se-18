package com.morozov.tm.command.user;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserLogoutCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private SessionEndpoint sessionEndpoint;

    public UserLogoutCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Close current user session";
    }

    @Override
    final public void execute() throws UserNotFoundException_Exception {
        final SessionDto session = serviceLocator.getSession();
        sessionEndpoint.closeSession(session);
        if (session != null) {
            ConsoleHelperUtil.writeString("Сессия закрыта");
        }
        serviceLocator.setSession(null);
    }
}
