package com.morozov.tm.command.system;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FindCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private TaskEndpoint taskEndpoint;
    @Autowired
    private ProjectEndpoint projectEndpoint;

    public FindCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @Override
    public String getName() {
        return "find";
    }

    @Override
    public String getDescription() {
        return "Find project and task by name or description";
    }

    @Override
    public void execute() throws AccessFirbidenException_Exception,
            RepositoryEmptyException_Exception, CloneNotSupportedException_Exception, ConnectionLostException_Exception,
            SqlCustomException_Exception, UserNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Введите строку для поиска");
        @NotNull final String findString = ConsoleHelperUtil.readString().trim();
        @NotNull final List<ProjectDto> projectsList = projectEndpoint
                .findProjectByStringInNameOrDescription(session, findString);
        @NotNull final List<TaskDto> taskList = taskEndpoint
                .findTaskByStringInNameOrDescription(session, findString);
        if (findString.isEmpty()) {
            ConsoleHelperUtil.writeString("Введенная строка пуста, будет выведен полный список проектов и задач");
        }
        if (!projectsList.isEmpty()) ConsoleHelperUtil.printProjectList(projectsList);
        else ConsoleHelperUtil.writeString("Проекты по строке поиска: " + findString + " не найдены");
        if (!taskList.isEmpty()) ConsoleHelperUtil.printTaskList(taskList);
        else ConsoleHelperUtil.writeString("Задачи по строке поиска: " + findString + " не найдены");
    }
}
