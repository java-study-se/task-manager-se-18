package com.morozov.tm.command.task;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskUpdateCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private TaskEndpoint taskEndpoint;

    public TaskUpdateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update selected task";
    }

    @Override
    final public void execute() throws StringEmptyException_Exception, CloneNotSupportedException_Exception,
            ParseException_Exception, TaskNotFoundException_Exception, AccessFirbidenException_Exception,
            UserNotFoundException_Exception, ProjectNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите ID задачи для изменения");
        @NotNull final String updateTaskID = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое имя задачи");
        @NotNull final String updateTaskName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое описание задачи");
        @NotNull final String updateTaskDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату начала задачи в формате DD.MM.YYYY");
        @NotNull final String startUpdateTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату окончания задачи в формате DD.MM.YYYY");
        @NotNull final String endUpdateTaskDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите ID проекта задачи");
        @NotNull final String updateTaskProjectId = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        taskEndpoint.updateTask(session, updateTaskID, updateTaskName, updateTaskDescription,
                startUpdateTaskDate, endUpdateTaskDate, updateTaskProjectId);
        ConsoleHelperUtil.writeString("Задача " + updateTaskName + " обновлена");
    }
}
