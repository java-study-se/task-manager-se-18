package com.morozov.tm.dto;

import com.morozov.tm.enumerated.UserRoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class SessionDto extends AbstractEntityDto implements Cloneable {
    @Nullable
    private String userId = null;
    @Nullable
    private String signature = null;
    @Nullable
    private Date timestamp = null;
    @Nullable
    private UserRoleEnum userRoleEnum = null;

    @Override
    public SessionDto clone() throws CloneNotSupportedException {
        return (SessionDto) super.clone();
    }
}
