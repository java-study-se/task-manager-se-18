package com.morozov.tm.util;


import com.morozov.tm.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class SessionUtil {

    @NotNull
    public static Session sign(@NotNull final Session session){
        @Nullable String value = session.getUser().getId()+session.getId();
        @NotNull final String salt = "soSiska";
        final int cycle = 2019;
        for (int i = 0; i < cycle; i++) {
            value = MD5HashUtil.getHash(salt + value + salt);
        }
        session.setSignature(value);
        return session;
    }
    public static boolean isSessionTimeLive(@Nullable final Date sessionTimeStamp){
        final long sessionTimeLiveMs = 1800000L;
        final long currentTimeMs = System.currentTimeMillis();
        if(sessionTimeStamp == null) return false;
        return ((currentTimeMs-sessionTimeStamp.getTime()) <= sessionTimeLiveMs);
    }
}
