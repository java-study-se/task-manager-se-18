package com.morozov.tm.enumerated;

public enum  FieldConst {
    ID,
    DATE_CREATE,
    DATE_END,
    DATE_START,
    DESCRIPTION,
    NAME,
    STATUS,
    USER_ID,
    SIGNATURE,
    TIMESTAMP,
    PROJECT_ID,
    PASSWORD_HASH,
    ROLE,
    LOGIN
}
