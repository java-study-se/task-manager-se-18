package com.morozov.tm.enumerated;

public enum StatusEnum {
    PLANNED("Запланировано", 1),
    PROGRESS("В процессе", 2),
    READY("Выполнена", 3);
    private String displayName;
    private int indexForCompare;

    public String getDisplayName() {
        return displayName;
    }

    public Integer getIndexForCompare() {
        return indexForCompare;
    }

    StatusEnum(String displayName, int indexForCompare) {
        this.displayName = displayName;
        this.indexForCompare = indexForCompare;
    }
}
