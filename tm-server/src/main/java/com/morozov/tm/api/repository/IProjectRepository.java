package com.morozov.tm.api.repository;


import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
    @Query("SELECT p FROM Project p WHERE p.user.id = ?1")
    List<Project> findByUser(@NotNull final String userId);

    @Query("SELECT p FROM Project p WHERE p.id = ?2 AND p.user.id = ?1")
    Optional<Project> findByUserAndId(@NotNull final String userId, @NotNull final String id);

    @Query("SELECT p FROM Project p WHERE p.user.id = ?1 AND (p.name LIKE CONCAT('%',?2,'%') OR p.description LIKE CONCAT('%',?2,'%'))")
    List<Project> searchProjectByString(@NotNull final String userId, @NotNull final String string);

    @Transactional
    @Modifying
    @Query("DELETE FROM Project p WHERE p.user.id = ?1")
    void removeAllByUserId(@NotNull final String userId);
}