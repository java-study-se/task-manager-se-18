package com.morozov.tm.api.repository;


import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends JpaRepository<Task, String> {
    @Query("SELECT t FROM Task t WHERE t.project.id = ?2 AND t.user.id = ?1")
    List<Task> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId);

    @Query("SELECT t FROM Task t WHERE t.user.id = ?1")
    List<Task> findAllByUserId(@NotNull final String userId);

    @Query("SELECT t FROM Task t WHERE t.id = ?2 and t.user.id = ?1")
    Optional<Task> findOneByUserId(@NotNull final String userId, @NotNull final String id);

    @Query("SELECT t FROM Task t WHERE t.project.id = ?1")
    List<Task> findAllTaskByProjectId(@NotNull final String projectId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Task t WHERE t.user.id = ?1 AND t.project.id = ?2")
    void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Query("SELECT t FROM Task t WHERE t.user.id = ?1 AND (t.name LIKE CONCAT ('%',?2,'%') OR t.description LIKE CONCAT ('%',?2,'%'))")
    List<Task> searchTaskByString(@NotNull final String userId, @NotNull final String string);

    @Transactional
    @Modifying
    @Query("DELETE FROM Task t WHERE t.user.id = ?1")
    void removeAllByUserId(@NotNull final String userId);
}
