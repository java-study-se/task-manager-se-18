package com.morozov.tm.endpoint;

import com.morozov.tm.api.service.ISessionService;
import com.morozov.tm.api.service.IUserService;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.service.Bootstrap;
import com.morozov.tm.util.ConsoleHelperUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
@Component
public class SessionEndpoint {
    @Autowired
    private IUserService userService;
    @Autowired
    private ISessionService sessionService;

    @WebMethod
    @Nullable
    public SessionDto openSession(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password)
            throws UserNotFoundException {
        @NotNull final User currentUser = userService.loginUser(login, password);
        @NotNull final Session session = sessionService.getNewSession(currentUser);
        @NotNull final SessionDto newSessionDto = sessionService.transferSessionToSessionDto(session, currentUser);
        System.out.println(String.format("Выдана сессия: \n Имя пользователя: %s \n ID сессии: %s \n ID клиента: %s \n",
                currentUser.getLogin(), newSessionDto.getId(), newSessionDto.getUserId()));
        return newSessionDto;
    }

    @WebMethod
    public void closeSession(
            @WebParam(name = "session") SessionDto sessionDto) throws UserNotFoundException {
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        sessionService.closeSession(session);
        ConsoleHelperUtil.writeString("Закрыта сессия ID " + sessionDto.getId());
    }

    @WebMethod
    public String serverPortInfo(
            @WebParam(name = "session") SessionDto sessionDto) throws AccessFirbidenException, UserNotFoundException,
            CloneNotSupportedException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        @NotNull StringBuilder port = new StringBuilder("8080");
        if (System.getProperty("server.port") != null) {
            port = new StringBuilder(System.getProperty("server.port"));
        }
        return port.toString();
    }
    @WebMethod
    public String serverHostInfo(
            @WebParam(name = "session") SessionDto sessionDto) throws AccessFirbidenException, UserNotFoundException,
            CloneNotSupportedException{
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        return Bootstrap.getHOST();
    }
}
