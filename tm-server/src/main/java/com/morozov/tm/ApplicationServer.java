package com.morozov.tm;


import com.morozov.tm.configuration.PersistenceJPAConfig;
import com.morozov.tm.service.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ApplicationServer {

    public static void main(String[] args) {
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(PersistenceJPAConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }
}
